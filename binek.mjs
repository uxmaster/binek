#!/usr/bin/env bun

import { argv, exit } from 'node:process';
import path from 'node:path';
import { writeFile } from 'node:fs';

if (argv.length < 3) {
	console.log(`
	usage:

	./binek.mjs <./schema.mjs>
	`); 
	exit(4);
}

const sizeOf = {
	char: 1,
	uint8_t: 1,
	int8_t: 1,
	uint16_t: 2,
	int16_t: 2,
	uint32_t: 4,
	int32_t: 4,
	float: 4,
	double: 8
};

const browserWrite = {
	uint8_t: 'setUint8',
	int8_t: 'setInt8',
	uint16_t: 'setUint16',
	int16_t: 'setInt16',
	uint32_t: 'setUint32',
	int32_t: 'setInt32',
	float: 'setFloat32',
	double: 'setFloat64'
};

const browserRead = {
	uint8_t: 'getUint8',
	int8_t: 'getInt8',
	uint16_t: 'getUint16',
	int16_t: 'getInt16',
	uint32_t: 'getUint32',
	int32_t: 'getInt32',
	float: 'getFloat32',
	double: 'getFloat64'
};

const nodeWrite = {
	uint8_t: 'writeUInt8',
	int8_t: 'writeInt8',
	uint16_t: 'writeUInt16LE',
	int16_t: 'writeInt16LE',
	uint32_t: 'writeUInt32LE',
	int32_t: 'writeInt32LE',
	float: 'writeFloatLE',
	double: 'writeDoubleLE'
};

const nodeRead = {
	uint8_t: 'readUInt8',
	int8_t: 'readInt8',
	uint16_t: 'readUInt16LE',
	int16_t: 'readInt16LE',
	uint32_t: 'readUInt32LE',
	int32_t: 'readInt32LE',
	float: 'readFloatLE',
	double: 'readDoubleLE'
};

function browserLE(type) {
	return sizeOf[type] > 1 ? ', true' : '';
}

import(path.resolve(argv[2])).then(m => {
	const schema = m.schema;
	const schemaName = path.basename(argv[2]).replace('.mjs','');
	const subschemas = {};

	let tab = '';
	let comment = '/**\n';
	let offset = 0;
	let hasSubschemas = false;
	let hasStrings = false;
	let hasDynamic = false;
	let schemaId;

	const processSchema = (schema, subschemaType, subschemaKey) => {
		let browserWriteCode = '';
		let browserReadCode = '';
		let nodeWriteCode = '';
		let nodeReadCode = '';
		let cWriteCode = '';
		let cReadCode = '';
		let size = 0;
		const dataKeys = [];
		let browserStrings = [];
		let browserDynamicSizes = [];
		let nodeStrings = [];
		let nodeDynamicSizes = [];
		let cDynamicSizes = [];

		const subTab = subschemaType === 'array' ? '\t' : '';
		const data = subschemaType === 'array' ? 'item' : 'data';
		const entries = Object.entries(schema);
		const numberOfEntries = entries.length;
		let index = 0;
		for (let [key, value] of entries) {
			let keyVar = key;
			let keyProp = key;
			if (subschemaKey) {
				keyVar = `${subschemaKey}_${key}`;
				keyProp = `${subschemaKey}.${key}`;
			}
			const updateOffset = subschemaType === 'array' || ++index < numberOfEntries;
			if (key === 'SCHEMA_ID' && !subschemaType) {
				browserWriteCode += `\tview.setUint8(0, ${value});\n`;
				nodeWriteCode += `\tbuf.writeUInt8(${value});\n`;
				cWriteCode += `\t*((uint8_t *)(buf)) = ${value};\n`;

				schemaId = value;
				size = 1;
				tab = '\t';
				offset = 1;
			} 
			else {
				let subschema;
				if (Array.isArray(value)) { // array of subschema structs
					[subschema, value] = value;
					value = 'array' + value;
				}
				else if (value instanceof Object) { // subschema
					const sub = processSchema(value, 'schema', key);
					dataKeys.push(`${key}: { ${sub.dataKeys.join(', ')} }`);
					browserWriteCode += sub.browserWriteCode;
					browserReadCode += sub.browserReadCode;
					nodeWriteCode += sub.nodeWriteCode;
					nodeReadCode += sub.nodeReadCode;
					cWriteCode += sub.cWriteCode;
					cReadCode += sub.cReadCode;
					size += sub.size;
					browserStrings = browserStrings.concat(sub.browserStrings);
					nodeStrings = nodeStrings.concat(sub.nodeStrings);
					browserDynamicSizes = browserDynamicSizes.concat(sub.browserDynamicSizes);
					nodeDynamicSizes = nodeDynamicSizes.concat(sub.nodeDynamicSizes);
					cDynamicSizes = cDynamicSizes.concat(sub.cDynamicSizes);
					continue;
				}

				dataKeys.push(`${key}: undefined`);
	
				if (value?.indexOf('[') > 0) { // array
					const [type, lengthType] = value.replace(']','').split('['); 
					let s = sizeOf[lengthType];
					let dynamic;
	
					if (s) { // dynamic size
						if (!sizeOf[type] && type !== 'array') throw `ERROR! Unknown type -> ${keyProp}: ${type}`; 
						if (!hasDynamic) { // first dynamic, define `offset`
							browserWriteCode += `\tlet offset = ${offset};\n`;
							browserReadCode += `\t${tab}let offset = ${offset};\n`;
							nodeWriteCode += `\tlet offset = ${offset};\n`;
							nodeReadCode += `\t${tab}let offset = ${offset};\n`;
							cWriteCode += `\tunsigned long offset = ${offset};\n`;
							cReadCode += `\t${tab}unsigned long offset = ${offset};\n`;
						}
						hasDynamic = dynamic = true;
						if (type === 'array') {
							hasSubschemas = true;
						}
						else {
							if (type === 'char') {
								hasStrings = true;
								browserStrings.push(`${subTab}\tconst ${keyVar}_bytes = textEncoder.encode(${data}.${keyProp});\n${subTab}\tconst ${keyVar}_len = ${keyVar}_bytes.length;\n`);
								nodeStrings.push(`${subTab}\tconst ${keyVar}_len = Buffer.byteLength(${data}.${keyProp});\n`);
								if (subschemaType === 'array') {
									browserDynamicSizes.push(`textEncoder.encode(${data}.${keyProp}).length`);	
									nodeDynamicSizes.push(`Buffer.byteLength(${data}.${keyProp})`);
								}
								else {
									browserDynamicSizes.push(`${keyVar}_len`);	
									nodeDynamicSizes.push(`${keyVar}_len`);
								}
							}
							else {
								browserDynamicSizes.push(`${data}.${keyProp}.length * ${sizeOf[type]}`);
								nodeDynamicSizes.push(`${data}.${keyProp}.length * ${sizeOf[type]}`);
							}
							cDynamicSizes.push(`${keyVar}_len`);
						}
					}
					else { // static size
						s = parseInt(lengthType, 10) * sizeOf[type];
						if (!s) throw `ERROR! Unknown type -> ${keyProp}: ${value}`;
					}
					size += s;
	
					if (dynamic)  { // dynamic size
						if (type === 'char') {
							browserWriteCode += `${subTab}\tview.${browserWrite[lengthType]}(offset, ${keyVar}_len${browserLE(lengthType)}); offset += ${s};\n${subTab}\tnew Uint8Array(buf, offset, ${keyVar}_len).set(${keyVar}_bytes);` + (updateOffset ? ` offset += ${keyVar}_len;\n` : '\n');
							browserReadCode += `${subTab}\t${tab}const ${keyVar}_len = view.${browserRead[lengthType]}(offset${browserLE(lengthType)}); offset += ${s};\n${subTab}\t${tab}${data}.${keyProp} = textDecoder.decode(new Uint8Array(buf, offset, ${keyVar}_len));` + (updateOffset ? ` offset += ${keyVar}_len;\n` : '\n');
							nodeWriteCode += `${subTab}\toffset = buf.${nodeWrite[lengthType]}(${keyVar}_len, offset);\n${subTab}\t` + (updateOffset ? `offset += ` : '') + `buf.write(${data}.${keyProp}, offset);\n`; 
							nodeReadCode += `${subTab}\t${tab}${data}.${keyProp} = buf.toString('utf8', offset + ${s}, ` + (updateOffset ? `offset +=` : 'offset +') + ` ${s} + buf.${nodeRead[lengthType]}(offset));\n`;
						}
						else if (type === 'array') {
							const sub = subschemas[keyProp] = processSchema(subschema, type);
							browserWriteCode += `\tview.${browserWrite[lengthType]}(offset, data.${keyProp}.length${browserLE(lengthType)}); offset += ${s};\n\tfor (const item of data.${keyProp}) {\n${sub.browserWriteCode}\t}\n`;
							browserReadCode += `\t${tab}const ${keyVar}_len = view.${browserRead[lengthType]}(offset${browserLE(lengthType)}); offset += ${s};\n\t${tab}data.${keyProp} = [];\n\t${tab}for (let i=0; i<${keyVar}_len; i++) {\n${sub.browserReadCode}\t${tab}\tdata.${keyProp}.push(item);\n\t${tab}}\n`;
							nodeWriteCode += `\toffset = buf.${nodeWrite[lengthType]}(data.${keyProp}.length, offset);\n\tfor (const item of data.${keyProp}) {\n${sub.nodeWriteCode}\t}\n`;
							nodeReadCode += `\t${tab}const ${keyVar}_len = buf.${nodeRead[lengthType]}(offset); offset += ${s};\n\t${tab}data.${keyProp} = [];\n\t${tab}for (let i=0; i<${keyVar}_len; i++) {\n${sub.nodeReadCode}\t${tab}\tdata.${keyProp}.push(item);\n\t${tab}}\n`;
							cWriteCode += `\t*(${lengthType} *)&buf[offset] = ${keyVar}_len; offset += ${s};\n\twhile (next(${keyProp}, &item)) {\n${sub.cWriteCode}\t}\n`;
							cReadCode += `\t${tab}${lengthType} *${keyVar}_len = (${lengthType} *)&buf[offset]; offset += ${s};\n\t${tab}for (${lengthType} i=0; i<${keyVar}_len; ++i) {\n${sub.cReadCode}\t${tab}}\n`;

							value = JSON.stringify(subschema, null, '\t\t').replaceAll('"', '').replace('}', '\t}') + `[${lengthType}]`;
						}
						else {
							browserWriteCode += `${subTab}\tview.${browserWrite[lengthType]}(offset, ${data}.${keyProp}.length${browserLE(lengthType)}); offset += ${s};\n${subTab}\tfor (const item of ${data}.${keyProp}) { view.${browserWrite[type]}(offset, item${browserLE(type)}); offset += ${sizeOf[type]}; }\n`;
							browserReadCode += `${subTab}\t${tab}const ${keyVar}_len = view.${browserRead[lengthType]}(offset${browserLE(lengthType)}); offset += ${s};\n${subTab}\t${tab}${data}.${keyProp} = []; for (let i=0; i<${keyVar}_len; i++) { ${data}.${keyProp}.push(view.${browserRead[type]}(offset${browserLE(type)})); offset += ${sizeOf[type]}; }\n`;
							nodeWriteCode += `${subTab}\toffset = buf.${nodeWrite[lengthType]}(${data}.${keyProp}.length, offset);\n${subTab}\tfor (const item of ${data}.${keyProp}) offset = buf.${nodeWrite[type]}(item, offset);\n`;
							nodeReadCode += `${subTab}\t${tab}const ${keyVar}_len = buf.${nodeRead[lengthType]}(offset); offset += ${s};\n${subTab}\t${tab}${data}.${keyProp} = []; for (let i=0; i<${keyVar}_len; i++) { ${data}.${keyProp}.push(buf.${nodeRead[type]}(offset)); offset += ${sizeOf[type]}; }\n`;
						}

						if (type !== 'array') {
							cWriteCode += `${subTab}\t*(${lengthType} *)&buf[offset] = ${keyVar}_len; offset += ${s};\n${subTab}\tmemcpy(buf+offset, ${subschemaType === 'array' ? data+'->'+keyVar : keyVar}, ${keyVar}_len${sizeOf[type] > 1 ? ` * ${sizeOf[type]}` : ''});` + (updateOffset ? ` offset += ${keyVar}_len * ${sizeOf[type]};\n` : '\n');
							cReadCode += `${subTab}\t${tab}${lengthType} *${keyVar}_len = (${lengthType} *)&buf[offset]; offset += ${s};\n${subTab}\t${tab}${type} *${keyVar} = (${type} *)&buf[offset];` + (updateOffset ? sizeOf[type] > 1 ? ` offset += *${keyVar}_len * ${sizeOf[type]};\n` : ` offset += *${keyVar}_len;\n` : '\n');
						}
					}
					else { // static size
						if (cDynamicSizes.length) {
							if (type === 'char') {
								browserWriteCode += `${subTab}\tnew Uint8Array(buf, offset, ${s}).set(${keyVar}_bytes);` + (updateOffset ? ` offset += ${s};\n` : '\n');
								browserReadCode += `${subTab}\t${tab}${data}.${keyProp} = textDecoder.decode(new Uint8Array(buf, offset, ${s}));` + (updateOffset ? ` offset += ${s};\n` : '\n');   
								nodeWriteCode += `${subTab}\tbuf.write(${data}.${keyProp}, offset, ${s});` + (updateOffset ? ` offset += ${s};\n` : '\n'); 
								nodeReadCode += `${subTab}\t${tab}${data}.${keyProp} = buf.toString('utf8', offset, ` + (updateOffset ? `offset +=` : 'offset +') + ` ${s});\n`;
							}
							else {
								browserWriteCode += `${subTab}\tfor (let i=0; i<${lengthType}; i++) { view.${browserWrite[type]}(${data}.${keyProp}[i], offset${browserLE(type)}); offset += ${sizeOf[type]}; };\n`;
								browserReadCode += `${subTab}\t${tab}${data}.${keyProp} = []; for (let i=0; i<${lengthType}; i++) { ${data}.${keyProp}.push(view.${browserRead[type]}(offset${browserLE(type)})); offset += ${sizeOf[type]}; }\n`;
								nodeWriteCode += `${subTab}\tfor (let i=0; i<${lengthType}; i++) offset = buf.${nodeWrite[type]}(${data}.${keyProp}[i], offset);\n`;
								nodeReadCode += `${subTab}\t${tab}${data}.${keyProp} = []; for (let i=0; i<${lengthType}; i++) { ${data}.${keyProp}.push(buf.${nodeRead[type]}(offset)); offset += ${sizeOf[type]}; }\n`;
							}
							cWriteCode += `${subTab}\tmemcpy(buf+offset, ${subschemaType === 'array' ? data+'->'+keyVar : keyVar}, ${s});` + (updateOffset ? ` offset += ${s};\n` : '\n');
							cReadCode += `${subTab}\t${tab}${type} *${keyProp} = (${type} *)&buf[offset];` + (updateOffset ? ` offset += ${s};\n` : '\n');
						}
						else {
							if (type === 'char') {
								browserWriteCode += `${subTab}\tnew Uint8Array(buf, ${offset}, ${s}).set(${keyVar}_bytes);\n`;
								browserReadCode += `${subTab}\t${tab}${data}.${keyProp} = textDecoder.decode(new Uint8Array(buf, offset, ${s}));\n`;   
								nodeWriteCode += `${subTab}\tbuf.write(${data}.${keyProp}, ${offset}, ${s});\n`; 
								nodeReadCode += `${subTab}\t${tab}${data}.${keyProp} = buf.toString('utf8', ${offset}, ${s});\n`;
							}
							else {
								browserWriteCode += `${subTab}\tfor (let i=0; i<${lengthType}; i++) view.${browserWrite[type]}(${data}.${keyProp}[i], ${offset} + i * ${sizeOf[type]}${browserLE(type)});\n`;
								browserReadCode += `${subTab}\t${tab}${data}.${keyProp} = []; for (let i=0; i<${lengthType}; i++) ${data}.${keyProp}.push(view.${browserRead[type]}(${offset} + i * ${sizeOf[type]}${browserLE(type)}));\n`;
								nodeWriteCode += `${subTab}\tfor (let i=0; i<${lengthType}; i++) buf.${nodeWrite[type]}(${data}.${keyProp}[i], ${offset} + i * ${sizeOf[type]});\n`;
								nodeReadCode += `${subTab}\t${tab}${data}.${keyProp} = []; for (let i=0; i<${lengthType}; i++) ${data}.${keyProp}.push(buf.${nodeRead[type]}(${offset} + i * ${sizeOf[type]}));\n`;
							}
							cWriteCode += `${subTab}\tmemcpy(buf+${offset}, ${subschemaType === 'array' ? data+'->'+keyVar : keyVar}, ${s});\n`;
							cReadCode += `${subTab}\t${tab}${type} *${keyVar} = (${type} *)&buf[${offset}];\n`;   
						}
						offset += s;
					}
				}
				else {
					const s = sizeOf[value];
					if (!s) throw `ERROR! Unknown type -> ${keyProp}: ${value}`;
					if (hasDynamic) {
						browserWriteCode += `${subTab}\tview.${browserWrite[value]}(offset, ${data}.${keyProp}${browserLE(value)});` + (updateOffset ? ` offset += ${s};\n` : '\n');
						browserReadCode += `${subTab}\t${tab}${data}.${keyProp} = view.${browserRead[value]}(offset${browserLE(value)});` + (updateOffset ? ` offset += ${s};\n` : '\n');  
						nodeWriteCode += `${subTab}\t` + (updateOffset ? `offset = ` : '') + `buf.${nodeWrite[value]}(${data}.${keyProp}, offset);\n`;
						nodeReadCode += `${subTab}\t${tab}${data}.${keyProp} = buf.${nodeRead[value]}(offset);` + (updateOffset ? ` offset += ${s};\n` : '\n');
						cWriteCode += `${subTab}\t*(${value} *)&buf[offset] = ${subschemaType === 'array' ? data+'->'+keyVar : keyVar};` + (updateOffset ? ` offset += ${s};\n` : '\n');
						cReadCode += `${subTab}\t${tab}${value} *${keyVar} = (${value} *)&buf[offset];` + (updateOffset ? ` offset += ${s};\n` : '\n');
					}
					else {
						browserWriteCode += `\tview.${browserWrite[value]}(${offset}, ${data}.${keyProp}${browserLE(value)});\n`;
						browserReadCode += `\t${tab}${data}.${keyProp} = view.${browserRead[value]}(${offset}${browserLE(value)});\n`;  
						nodeWriteCode += `\tbuf.${nodeWrite[value]}(${data}.${keyProp}, ${offset});\n`;
						nodeReadCode += `\t${tab}${data}.${keyProp} = buf.${nodeRead[value]}(${offset});\n`;
						cWriteCode += `\t*(${value} *)&buf[${offset}] = ${subschemaType === 'array' ? data+'->'+key : key};\n`;
						cReadCode += `\t${tab}${value} *${keyProp} = (${value} *)&buf[${offset}];\n`;
					}
					size += s;
					offset += s;
				}
			}
			if (subschemaType !== 'array') comment += `\t${keyProp}: ${value}\n`;
		}

		if (subschemaType === 'array' && browserStrings.length) {
			browserWriteCode = browserStrings.join('') + browserWriteCode;
			nodeWriteCode = nodeStrings.join('') + nodeWriteCode;
		}
		if (subschemaType !== 'schema') {
			browserReadCode = `${subTab}\t${tab}${subschemaType || schemaId === undefined ? 'const ' : ''}${data} = { ${dataKeys.join(', ')} };\n` + browserReadCode;
			nodeReadCode = `${subTab}\t${tab}${subschemaType || schemaId === undefined ? 'const ' : ''}${data} = { ${dataKeys.join(', ')} };\n` + nodeReadCode;
		}

		return {
			browserWriteCode,
			browserReadCode,
			nodeWriteCode,
			nodeReadCode,
			cWriteCode,
			cReadCode,
			size,
			dataKeys,
			browserStrings,
			browserDynamicSizes,
			nodeStrings,
			nodeDynamicSizes,
			cDynamicSizes,
		};
	};

	let {
		browserWriteCode,
		browserReadCode,
		nodeWriteCode,
		nodeReadCode,
		cWriteCode,
		cReadCode,
		size,
		dataKeys,
		browserStrings,
		browserDynamicSizes,
		nodeStrings,
		nodeDynamicSizes,
		cDynamicSizes,
	} = processSchema(schema);

	if (tab) {
		browserReadCode += `\t}\n\telse throw new Error('UnknownSchema');\n`; 
		nodeReadCode += `\t}\n\telse throw new Error('UnknownSchema');\n`;
		cReadCode += `\t}\n\telse {}\n`; 
	}

	let browserWriteCodePre = `function ${schemaName}Encode(data`;
	let browserReadCodePre = `function ${schemaName}Decode(buf`;
	let nodeWriteCodePre = `import { Buffer } from 'node:buffer';\n\nexport function ${schemaName}Encode(data) {\n`;

	if (hasStrings) {
		browserWriteCodePre += `, textEncoder) {\n`;
		browserReadCodePre +=  `, textDecoder) {\n\tconst view = new DataView(buf);\n`;
	}
	else {
		browserWriteCodePre += `) {\n`;
		browserReadCodePre += `) {\n\tconst view = new DataView(buf);\n`;
	}
	if (schemaId !== undefined) {
		browserReadCodePre += `\tconst schemaId = view.getUint8(0);\n\tlet data;\n\tif (schemaId === ${schemaId}) {\n`;
		nodeReadCode = `\tconst schemaId = buf.readUInt8();\n\tlet data;\n\tif (schemaId === ${schemaId}) {\n` + nodeReadCode;
		cReadCode = `\tunsigned char schema_id = buf[0];\n\tif (schema_id == ${schemaId}) {\n` + cReadCode;
	}

	if (hasSubschemas) {
		for (const keyProp in subschemas) {
			const sub = subschemas[keyProp];
			const keyVar = keyProp.replaceAll('.', '_');
			if (sub.browserDynamicSizes.length) {
				browserWriteCodePre += `\tlet ${keyVar}_len = 0;\n\tfor (const item of data.${keyProp}) {\n\t\t${keyVar}_len += ${sub.size} + ${sub.browserDynamicSizes.join(' + ')};\n\t}\n`;
				nodeWriteCodePre += `\tlet ${keyVar}_len = 0;\n\tfor (const item of data.${keyProp}) {\n\t\t${keyVar}_len += ${sub.size} + ${sub.nodeDynamicSizes.join(' + ')};\n\t}\n`;
				browserDynamicSizes.push(`${keyVar}_len`);
				nodeDynamicSizes.push(`${keyVar}_len`);
				cDynamicSizes.push(`${keyVar}_len`);
			}
			else {
				browserDynamicSizes.push(`data.${keyProp}.length * ${sub.size}`);
				nodeDynamicSizes.push(`data.${keyProp}.length * ${sub.size}`);
			}
		}
	}

	browserWriteCodePre += browserStrings.join('') + `\tconst buf = new ArrayBuffer(${size}`;
	browserWriteCodePre += browserDynamicSizes.length ? ` + ${browserDynamicSizes.join(' + ')});\n` : ');\n';
	
	browserWriteCode = browserWriteCodePre + '\tconst view = new DataView(buf);\n' + browserWriteCode + '\treturn buf;\n}\n';
	browserReadCode = browserReadCodePre + browserReadCode + '\treturn data;\n}\n';
	
	nodeReadCode = `export function ${schemaName}Decode(buf) {\n` + nodeReadCode;

	nodeWriteCodePre += nodeStrings.join('') + `\tconst buf = Buffer.allocUnsafe(${size}`;
	nodeWriteCodePre += nodeDynamicSizes.length ? ` + ${nodeDynamicSizes.join(' + ')});\n` : ');\n';
   
	nodeWriteCode = nodeWriteCodePre + nodeWriteCode + '\treturn buf;\n}\n';
	nodeReadCode += '\treturn data;\n}\n';

	cWriteCode = `\tchar *buf = malloc(${size}` + (cDynamicSizes.length ? ` + ${cDynamicSizes.join(' + ')});\n` : ');\n') + cWriteCode;

	comment = '/*' + JSON.stringify(schema, undefined, '\t').replace(/"(.*)":/gm, '$1:').replaceAll('"', "'");

	writeFile(schemaName + 'Bin.js', comment + '*/\n\n' + browserWriteCode + '\n' + browserReadCode + `\n\nwindow.${schemaName}Encode = ${schemaName}Encode;\nwindow.${schemaName}Decode = ${schemaName}Decode;\n`, (err) => {
		if (err) throw err;
		console.log(`The file ${schemaName + 'Bin.js'} has been created. Use it in a web browser.`);
	});

	writeFile(schemaName + 'Bin.mjs', comment + '*/\n\n' + nodeWriteCode + '\n' + nodeReadCode, (err) => {
		if (err) throw err;
		console.log(`The file ${schemaName + 'Bin.mjs'} has been created. Use it in Bun.`);
	});

	writeFile(schemaName + 'Bin.c', comment + '*/\n\n\n\t/* encode */\n' + cWriteCode + '\n\n\t/* decode */\n' + cReadCode, (err) => {
		if (err) throw err;
		console.log(`The file ${schemaName + 'Bin.c'} has been created. Use it in C.`);
	}); 

}).catch(e => console.error(e));

