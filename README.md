# Binek

Binek, given a schema, creates some code in C and JavaScript (web browsers & [Bun](https://bun.sh/)) to encode and decode the binary representation of data assuming little-endian.

## Getting started

```javascript
export const schema = {
   SCHEMA_ID: 1,           // optional schema version number (uint8_t), comes first   
   flags: 'uint32_t[3]',   // array containing exactly three uint32_t values
   number: 'double',
   page: 'uint16_t',
   name: 'char[uint16_t]', // not null-terminated with length expressed as uint16_t
   meeting: {              // nested object            
      id: 'uint32_t',
      participants: [{     // array of objects...
         id: 'uint32_t',
         state: 'uint8_t'
      }, '[uint16_t]']     // ... with length expressed as uint16_t
   }
};
```

```
./binek.mjs ./schema.mjs
```

If you use `char[]` in a web browser, generated functions require instances of TextEncoder and TextDecoder as arguments:
```javascript
const textEncoder = new TextEncoder();
const textDecoder = new TextDecoder();

schemaEncode(data, textEncoder);
schemaDecode(buffer, textDecoder);
```

## Status

It is an experimental version I use for my projects. There is no warranty of any kind (see the [MIT License](https://opensource.org/licenses/MIT)).